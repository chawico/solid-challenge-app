package com.dojo.iv.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(VeiculoController.class)
public class VeiculoControllerTest {

  @Autowired
  private MockMvc mvc;
  
  @Test
  public void calculoIVCarro() throws Exception {
    // @formatter:off
    mvc.perform(
        MockMvcRequestBuilders
          .get("/caminho")
          .param("nome", "123")
          .accept(MediaType.APPLICATION_JSON_UTF8))
    .andDo(print())
    .andExpect(status().isOk())
    .andExpect(MockMvcResultMatchers.content().string("ok"));
 
    // @formatter:on

  }
}
