package com.dojo.iv.service;

import java.util.UUID;
import org.springframework.stereotype.Service;

@Service
public class VeiculoService {

  public String pagar(String registro) {
    return UUID.fromString(registro).toString();
  }
}
